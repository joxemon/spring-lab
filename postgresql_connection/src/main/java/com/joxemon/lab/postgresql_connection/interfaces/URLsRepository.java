package com.joxemon.lab.postgresql_connection.interfaces;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import com.joxemon.lab.postgresql_connection.model.URLs;

public interface URLsRepository extends CrudRepository<URLs, Long> {

	public List<URLs> findByUrl(String url);
}
