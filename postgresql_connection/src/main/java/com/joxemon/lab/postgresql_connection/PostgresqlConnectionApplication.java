package com.joxemon.lab.postgresql_connection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.joxemon.lab.postgresql_connection.interfaces.URLsRepository;
import com.joxemon.lab.postgresql_connection.model.URLs;


@SpringBootApplication
public class PostgresqlConnectionApplication {
	
	private static final Logger log = LoggerFactory.getLogger(PostgresqlConnectionApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(PostgresqlConnectionApplication.class, args);
	}
	
	@Bean
	public CommandLineRunner demo(URLsRepository repository) {
		return (args) -> {
			// save a couple of urls
			
			
			repository.save(new URLs("http://www.google.es"));
			repository.save(new URLs("http://www.marca.es"));
			repository.save(new URLs("http://www.publico.es"));
			repository.save(new URLs("http://www.danone.es"));
			repository.save(new URLs("http://www.cocacola.es"));

			
		
			// fetch all Urls
			
			log.info("Urls found with findAll():");
			log.info("-------------------------------");
			repository.findAll().forEach( url -> {
				log.info(url.toString());
				
			}); 
				
			
			log.info("");
			

			// fetch an individual Url by ID
			repository.findById(1302L)
				.ifPresent(url -> {
					log.info("Customer found with findById(1302L):");
					log.info("--------------------------------");
					log.info(url.toString());
					log.info("");
					
				});

			// fetch urls by Url
			log.info("Urls found with findByUrl('http://www.publico.es'):");
			log.info("--------------------------------------------");
			repository.findByUrl("http://www.publico.es").forEach(url -> {
				log.info(url.toString());
			});
	
			log.info("");
			
			
			// update urls by last name
			repository.findByUrl("http://www.marca.es").forEach(ur -> {
				ur.setFirma("Jx1");
				repository.save(ur);
			});
		};
	}

}
