package com.joxemon.lab.postgresql_connection.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@Entity(name="Urls")
@RequiredArgsConstructor
@SequenceGenerator(name="seq", initialValue=1, allocationSize=100)
public class URLs implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
	private Long id;
	@NonNull
	private String url;
	private Boolean flagActivo = false;
	
	private Date created;
	private Date updated;
	private String firma = "Jx";

	  @PrePersist
	  protected void onCreate() {
	    created = new Date(System.currentTimeMillis());
	  }

	  @PreUpdate
	  protected void onUpdate() {
	    updated = new Date(System.currentTimeMillis());
	  }
	  
	  

}
